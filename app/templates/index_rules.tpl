<table class="main" width="100%" align="center">
	<tr>
		<td>
			<h2>Regels</h2>
			<script type="text/javascript">//<![CDATA[
				/**
				 * @jQuery
				 */
				function toggleRule(rule_number) {ldelim}
					$('#more_'+rule_number).slideToggle();
					$('#link_'+rule_number).slideToggle();
					return false;
				{rdelim}
			//]]></script>
			<h4>§1) Een account per wereld</h4>
			<p>Het is niet toegestaan om meer dan 1 account te hebben per wereld.<br />
			<div id="more_1" style="display: none">
				<ul>
					<li> Het is verboden om wachtwoorden del met met spelers op tenelfde wereld. </li>
					<li> If a player sends you their password, you must report the message immediately! Otherwise both accounts will be blocked. </li>
					<li> It is forbidden for more than one player to access the account at the same time. Even with declared co-play, access can only be done by one player at a time. </li>
					<li> Multiple people are allowed to play from the same computer, but each player should only play for themselves and only on their account. Each must have their own password and one must not know the other's password! <b> See rule §2 </b>. </li>
					<li> It is forbidden to access the account of a partner, friend, relative, among others and replace it until it is available. You should NEVER log in to another player's account in the same world where you play using their password. If you need to sign in to someone's account, it must be through Vacation Mode (Settings> Vacation Mode). No apology will be accepted for violating this rule. </li>
				</ul>
			</div>
			<span id="link_"><a id="link_1" href="?rule=1" onclick="return toggleRule(1);" style="font-size: x-small">&raquo; Meer</a></span>
			</p>
			<h4>§2) Interações entre contas na mesma conexão</h4>
			<p>É proibido que contas na mesma conexão interajam. Para os devidos efeitos, será considerada partilha de conexão quando houver logins com períodos superiores à 24 horas, ou com freqüência elevada da partilha de conexão, como contas na mesma conexão. Ou seja, 3 pessoas que morem na mesma residência podem acessar contas no mesmo mundo, porém a partilha deve ser informada (Configurações > Compartilhar conexão à (nternet) em todas as contas. Caso ocorra interação, todos os envolvidos serão penalizados.<br />
			<div id="more_2" style="display: none">
				<h4>Exemplos:</h4>
				<ul>
					<li> Players sharing the same internet connection cannot send support troops or resources to the same player. </li>
					<li> Players sharing the same internet connection cannot attack the same player or village. </li>
					<li> Players sharing the same internet connection can attack the same tribe. </li>
					<li> Players using the same connection cannot attack, support or trade resources. </li>
					<li> It is prohibited to exchange resources between vacation mode accounts and other accounts that share the same internet connection. </li>
					<li> Accounts in the same connection are not allowed to help each other at all. </li>
					<li> Using a third account to transfer resources or villages between accounts on the same connection is prohibited. In this case, the 3 accounts are breaking the rules. </li>
					<li> Using an account to hack attacks against another account using the same connection is prohibited. That is, only the account that was attacked can fight back. </li>
				</ul>
			</div>
			<span id="link_"><a id="link_2" href="?rule=2" onclick="return toggleRule(2);" style="font-size: x-small">&raquo; mais</a></span>
			</p>
			<h4>§3) Modo de férias</h4>
				<p> Vacation mode is a way to take care of another player's account while they are away. The restrictions discussed in <b> §2 </b> are enforced in the event of vacation substitution and are valid for all accounts played by the substitute concerned, ie interactions between accounts managed by the same substitute (including the account). substitute) are prohibited. Sending support to them is also prohibited. Substitutes who clearly perform destructive actions against the vacation account will be punished. <br /> <br />
				<b> Any kind of interaction is only allowed 24 hours after vacation mode ends. </b> <br /> <br />
				You cannot use an account in vacation mode to benefit another account. The account owner and substitute are equally responsible for any action taken on the account while in vacation mode. Only accept invitations to vacation mode if you trust the player, the same applies when sending the invitation to someone. Vacation mode is temporary and should not be used permanently. The maximum period is 120 days! After 120 days, the support team may cancel vacation mode. <br />
			<div id="more_3" style="display: none">
				<h4>Exemplos:</h4>
				<ul>
					<li> Attacking and conquering other villages is allowed while on vacation replacement for this account. </li>
					<li> It is prohibited to support or send resources to accounts with which you share your internet connection or to which you are replacing. </li>
					<li> Players who share the same internet connection as you cannot attack the account you are replacing. </li>
					<li> It is forbidden to send support to a player from your account and the account you are a replacement for. </li>
					<li> It is permissible to attack multiple players of the same tribe with your account and the account of which you are a substitute, provided that each account attacks its own target (player) in isolation without any interaction between your account and the account of which you are a substitute. you are a replacement. </li>
					<li> It is prohibited to use an account that is in vacation mode to write a message to another player to check if he is online or not. </li>
					<li> After 60 days in vacation mode, the substitute may allow other players to conquer the villages, but will not be able to remove any troops from the villages! All troops in the village must be killed by the attacker. If the troops are in support of another village, they must be called back and killed by the conquering village. Failure to follow this process will lead to blocking of all involved. The only exception to this is if the account owner submits a ticket to support saying that the substitute is free to donate the villages immediately. </li>
				</ul>
			</div>
			<span id="link_"><a id="link_3" href="?rule=3" onclick="return toggleRule(3);" style="font-size: x-small">&raquo; mais</a></span>
			</p>
			<h4>§4) Expressive Modes</h4>
			<p> Offenses against other players are prohibited. Threats or extortion can only be exercised within the context of the game. Threats to physical integrity are prohibited. Any expression of political, pornographic extremism or any other unlawful context is strictly prohibited. Abbreviations of offenses are prohibited, including those of double meaning. <br />			<div id="more_4" style="display: none">
				<h4>Examples:</h4>
				<ul>
					<li> Only offenses will be allowed within the context of the game, such as noob, traitor, coward, defector, etc. </li>
					<li> Blackmail is strictly prohibited in exchange for Premium Points. </li>
					<li> Providing links to pornographic content is prohibited. The same applies to the profile. </li>
					<li> Posting for advertising purposes from other commercial or personal websites is prohibited. The same applies to the profile. Exceptions only for legal and relationship media sites, such as youtube, twitter, facebook, among others. The support team is not responsible for opening external pages and their content. </li>
					<li> Account unlocking campaigns are prohibited and will lead to the blocking of everyone involved. </li>
					<li> The publication of material of a racist, offensive, profanity, discriminatory nature or in violation of the laws in force in the country is prohibited. All media is prohibited, such as private message, username, profile. </li>
					<li> Supporting insults will lead to immediate account ban. </li>
					<li> Misuse and abuse of the support system could result in account ban. </li>
					<li> Tribe forums are the responsibility of tribe aristocrats, not the support staff. But in extreme cases, such as material containing pedophilia, the support team will intervene and severely punish those who posted the material. Management reserves the right to intervene in the case of pornographic materials, cheats and distribution of any illegal materials. </li>
<li> The Portuguese language is spoken in different countries and many expressions, terms and words have different meanings according to each country but will be judged with the meaning in Brazil. </li>
				</ul>
			</div>
			<span id="link_"><a id="link_4" href="?rule=4" onclick="return toggleRule(4);" style="font-size: x-small">&raquo; mais</a></span>
			</p>
		</td>
	</tr>
</table>